KUBECTL=kubectl --context kubernetes-admin@kubernetes
HELM=helm upgrade --install --kube-context kubernetes-admin@kubernetes

HELM_NGINX_INGRESS_CONTROLLER_VERSION=1.29.5
HELM_PROM_OPERATOR_VERSION=8.5.14

nginx:
	$(HELM) --namespace nginx nginx stable/nginx-ingress \
					--version=$(HELM_NGINX_INGRESS_CONTROLLER_VERSION) \
				  -f nginx/nginx-ingress-controller.yaml

monitoring:
	$(HELM) --namespace monitoring prometheus-operator stable/prometheus-operator \
					--version=$(HELM_PROM_OPERATOR_VERSION) \
					-f monitoring/helm/prometheus-operator.yaml

.PHONY: monitoring nginx init
