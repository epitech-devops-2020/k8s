# k8s

## Requirements

- RKE       -> https://rancher.com/docs/rke/latest/en/installation/
- docker    -> https://docs.docker.com/install/linux/docker-ce/ubuntu/
- kubectl   -> https://kubernetes.io/fr/docs/tasks/tools/install-kubectl/

## Installation

- Generate your config file with `rke config --name cluster.yml`
- Start your cluster with `rke up`

## Configuration

- Add the kube context generated in the `kube_config_cluster.yml` file in your kubectl config

## Usage

- After your cluster is up and running, you can start using the generated kubeconfig file to start interacting with your Kubernetes cluster using kubectl